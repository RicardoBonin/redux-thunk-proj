import React, { Component } from 'react'
import Info from './Info'
import UserAgent from './UserAgent'
import { createStore, applyMiddleware } from 'redux'
import reducer from './reducers/index'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'

const store = createStore(
  reducer,
  applyMiddleware(logger, thunk),
  
)

class App extends Component {
  render(){
    return (
      <Provider store={store}>
        <div className="App">
          <Info />
          <UserAgent />
        </div>
      </Provider>
    )
  }
}

export default App;
